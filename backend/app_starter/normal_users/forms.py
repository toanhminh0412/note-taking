# pylint:
from django.forms import ModelForm

from .models import Workspace, DocList, Doc

# Form to validate and process POST requests for workspaces
class WorkspaceForm(ModelForm):
    class Meta:
        model = Workspace
        fields = ['name']
        
# Form to validate and process POST requests for doc lists
class DocListForm(ModelForm):
    class Meta:
        model = DocList
        fields = ['name', 'workspace']
        
# Form to validate and process POST requests for docs
class DocForm(ModelForm):
    class Meta:
        model = Doc
        fields = ['name', 'doc_list', 'content', 'tags']
