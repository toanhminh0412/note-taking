# pylint:
import json

from django.views import View
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models import Q

from .models import Workspace, DocList, Doc
from .forms import WorkspaceForm, DocListForm, DocForm

# Temporary disable CSRF protection for proof of concept
@method_decorator(csrf_exempt, name='dispatch')
class WorkspaceAPIView(View):
    """
    Handle API calls to manipulate workspaces
    """
    def get(self, request, **kwargs):
        """
        Get all workspaces
        """
        # User must be authenticated to get workspaces
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to get workspaces!"
            }, status=401)

        # Get a specific workspace
        if workspace_id := kwargs.get('workspace_id', None):
            if request.path == reverse('get_workspace', kwargs={'workspace_id': workspace_id}):
                workspace = Workspace.objects.get(id=workspace_id)
                if workspace.created_by != request.user and request.user not in workspace.members.all():
                    return JsonResponse({
                        "message": "You do not have permission to access this workspace!"
                    }, status=403)

                # Return workspace
                return JsonResponse({
                    "workspace": workspace.to_dict()
                }, status=200)

        # Get all workspaces
        if request.path == reverse('get_all_workspaces'):

            # Workspaces created by user
            workspaces = Workspace.objects.filter(created_by=request.user)
            workspaces = [workspace.to_dict() for workspace in workspaces]

            # Workspaces shared with user
            shared_workspaces = request.user.workspaces.all()
            shared_workspaces = [workspace.to_dict() for workspace in shared_workspaces]

            workspaces.extend(shared_workspaces)

            # Return workspaces
            return JsonResponse({
                "workspaces": workspaces
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


    def post(self, request, **kwargs):
        """
        Create a new workspace
        """
        # User must be authenticated to create a workspace
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to create a workspace!"
            }, status=401)

        # Create a new workspace
        data = json.loads(request.body)
        form = WorkspaceForm(data)
        if form.is_valid():
            workspace = form.save(commit=False)
            workspace.created_by = request.user
            workspace.save()

            # Return success message
            return JsonResponse({
                "message": f"Create workspace {form.cleaned_data['name']} successfully!",
                "workspace": workspace.to_dict()
            }, status=201)

        # Return error message if POST data is invalid
        return JsonResponse({
            "message": f"Failed to create workspace!",
            'errors': form.errors
        }, status=400)

    def put(self, request, **kwargs):
        """
        Update a workspace
        """
        # User must be authenticated to update a workspace
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to update a workspace!"
            }, status=401)

        # Must provide workspace_id as a param in path
        if 'workspace_id' not in kwargs:
            return JsonResponse({
                "message": "Missing workspace id"
            }, status=400)

        workspace_id = kwargs['workspace_id']

        # Update a workspace
        if request.path == reverse('update_workspace', kwargs={'workspace_id': workspace_id}):
            data = json.loads(request.body)
            workspace = Workspace.objects.get(id=workspace_id)
            if workspace.created_by != request.user and request.user not in workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to update this workspace!"
                }, status=403)

            form = WorkspaceForm(data, instance=workspace)
            if form.is_valid():
                workspace = form.save()
                workspace.save()

                # Return success message
                return JsonResponse({
                    "message": f"Update workspace {form.cleaned_data['name']} successfully!",
                    "workspace": workspace.to_dict()
                }, status=200)

            # Return error message if PUT data is invalid
            return JsonResponse({
                "message": f"Failed to update workspace!",
                'errors': form.errors
            }, status=400)

        # Update members in a workspace
        if request.path == reverse('update_workspace_members', kwargs={'workspace_id': workspace_id}):
            data = json.loads(request.body)
            workspace = Workspace.objects.get(id=workspace_id)
            if workspace.created_by != request.user:
                return JsonResponse({
                    "message": "You do not have permission to update this workspace's members!"
                }, status=403)

            members = data.get('members', [])
            # Remove all members before adding new members to workspace
            workspace.members.clear()

            # Add members to workspace if found an user with the provided email
            for member in members:
                if user := User.objects.filter(username=member).first():
                    workspace.members.add(user)
            workspace.save()

            # Return success message
            return JsonResponse({
                "message": f"Update workspace members successfully!",
                "workspace": workspace.to_dict()
            }, status=200)

        # Update position of doc lists in a workspace
        if request.path == reverse('update_doc_lists_position', kwargs={'workspace_id': workspace_id}):
            data = json.loads(request.body)
            workspace = Workspace.objects.get(id=workspace_id)
            if workspace.created_by != request.user and request.user not in workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to update this workspace's doc lists position!"
                }, status=403)

            doc_list_ids = data.get('docListIds', [])
            workspace.arrage_doc_lists(doc_list_ids)

            # Return success message
            return JsonResponse({
                "message": f"Update doc lists position successfully!",
                "workspace": workspace.to_dict()
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)

    def delete(self, request, **kwargs):
        """
        Delete a workspace
        """
        # User must be authenticated to delete a workspace
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to delete a workspace!"
            }, status=401)

        # Must provide workspace_id as a param in path
        if 'workspace_id' not in kwargs:
            return JsonResponse({
                "message": "Missing workspace id"
            }, status=400)

        workspace_id = kwargs['workspace_id']

        # Delete a workspace
        if request.path == reverse('delete_workspace', kwargs={'workspace_id': workspace_id}):
            workspace = Workspace.objects.get(id=workspace_id)
            if workspace.created_by != request.user:
                return JsonResponse({
                    "message": "You do not have permission to delete this workspace!"
                }, status=403)

            workspace.delete()

            # Return success message
            return JsonResponse({
                "message": "Delete workspace successfully!"
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


# Temporary disable CSRF protection for proof of concept
@method_decorator(csrf_exempt, name='dispatch')
class DocListAPIView(View):
    """
    Handle API calls to manipulate doc lists
    """
    def get(self, request, **kwargs):
        """
        Get all doc lists in a workspace
        """
        # User must be authenticated to get doc lists
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to get doc lists!"
            }, status=401)

        # Get all doc lists in a workspace
        if workspace_id := kwargs.get('workspace_id', None):
            if request.path == reverse('get_all_doc_lists_from_workspace', kwargs={'workspace_id': workspace_id}):
                workspace = Workspace.objects.get(id=workspace_id)
                if workspace.created_by != request.user and request.user not in workspace.members.all():
                    return JsonResponse({
                        "message": "You do not have permission to access this workspace!"
                    }, status=403)

                doc_lists = workspace.get_doc_lists()

                # Return doc lists
                return JsonResponse({
                    "doc_lists": doc_lists
                }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


    def post(self, request):
        """
        Create a new doc list
        """
        # User must be authenticated to create a doc list
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to create a doc list!"
            }, status=401)

        # Create a new doc list
        data = json.loads(request.body)
        form = DocListForm(data)
        if form.is_valid():
            # User must either be the owner of the workspace or a member of the workspace
            workspace = form.cleaned_data['workspace']
            if workspace.created_by != request.user and request.user not in workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to create a doc list in this workspace!"
                }, status=403)

            doc_list = form.save()
            doc_list.save()

            # Return success message
            return JsonResponse({
                "message": f"Create doc list {form.cleaned_data['name']} successfully!",
                "doc_list": doc_list.to_dict()
            }, status=201)

        # Return error message if POST data is invalid
        return JsonResponse({
            "message": f"Failed to create doc list!",
            'errors': form.errors
        }, status=400)

    def put(self, request, **kwargs):
        """
        Update a doc list
        """
        # User must be authenticated to update a doc list
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to update a doc list!"
            }, status=401)

        # Must provide doc_list_id as a param in path
        if 'doc_list_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc list id"
            }, status=400)

        doc_list_id = kwargs['doc_list_id']

        # Update a doc list
        if request.path == reverse('update_doc_lists', kwargs={'doc_list_id': doc_list_id}):
            data = json.loads(request.body)

            try:
                doc_list = DocList.objects.get(id=doc_list_id)
            except DocList.DoesNotExist:
                return JsonResponse({
                    "message": "Doc list not found!"
                }, status=404)

            if doc_list.workspace.created_by != request.user and request.user not in doc_list.workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to update this doc list!"
                }, status=403)

            form = DocListForm(data, instance=doc_list)
            if form.is_valid():
                doc_list = form.save()
                doc_list.save()

                # Return success message
                return JsonResponse({
                    "message": f"Update doc list {form.cleaned_data['name']} successfully!",
                    "doc_list": doc_list.to_dict()
                }, status=200)

            # Return error message if PUT data is invalid
            return JsonResponse({
                "message": "Failed to update doc list!",
                'errors': form.errors
            }, status=400)

        # Update position of a doc in a doc list
        if request.path == reverse('update_docs_position', kwargs={'doc_list_id': doc_list_id}):            
            data = json.loads(request.body)
            doc_list = DocList.objects.get(id=doc_list_id)
            if doc_list.workspace.created_by != request.user and request.user not in doc_list.workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to update this doc list's position!"
                }, status=403)

            doc_ids = data.get('docIds', [])
            doc_list.arrange_docs(doc_ids)

            # Return success message
            return JsonResponse({
                "message": "Update doc position successfully!",
                "doc_list": doc_list.to_dict()
            }, status=200)

        # Move a doc to another doc list
        if request.path == reverse('move_doc_to_list', kwargs={'doc_list_id': doc_list_id}):            
            data = json.loads(request.body)
            doc_id = data.get('docId', None)
            if not doc_id:
                return JsonResponse({
                    "message": "Missing doc id!"
                }, status=400)

            doc = Doc.objects.get(id=doc_id)
            source_doc_list = doc.doc_list
            destination_doc_list = DocList.objects.get(id=doc_list_id)

            # User must be the owner of the old doc list or a member of the old doc list
            # User must be the owner of the new doc list or a member of the new doc list
            if (
                (source_doc_list.workspace.created_by != request.user
                and request.user not in source_doc_list.workspace.members.all())
                or
                (destination_doc_list.workspace.created_by != request.user
                and request.user not in destination_doc_list.workspace.members.all())):
                return JsonResponse({
                    "message": "You do not have permission to move this doc!"
                }, status=403)

            doc.doc_list = destination_doc_list
            doc.save()

            source_doc_ids = data.get('sourceDocIds', [])
            destination_doc_ids = data.get('destinationDocIds', [])
            source_doc_list.arrange_docs(source_doc_ids)
            destination_doc_list.arrange_docs(destination_doc_ids)

            # Return success message
            return JsonResponse({
                "message": "Move doc successfully!",
                "doc": doc.to_dict()
            }, status=200)


        # Invalid path
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)
        
    def delete(self, request, **kwargs):
        """
        Delete a doc list
        """
        # User must be authenticated to delete a doc list
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to delete a doc list!"
            }, status=401)

        # Must provide doc_list_id as a param in path
        if 'doc_list_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc list id"
            }, status=400)

        doc_list_id = kwargs['doc_list_id']

        # Delete a doc list
        if request.path == reverse('delete_doc_lists', kwargs={'doc_list_id': doc_list_id}):
            doc_list = DocList.objects.get(id=doc_list_id)
            if doc_list.workspace.created_by != request.user and request.user not in doc_list.workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to delete this doc list!"
                }, status=403)

            doc_list.delete()

            # Return success message
            return JsonResponse({
                "message": "Delete doc list successfully!"
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)


# Temporary disable CSRF protection for proof of concept
@method_decorator(csrf_exempt, name='dispatch')
class DocAPIView(View):
    """
    Handle API calls to manipulate docs
    """
    def get(self, request, **kwargs):
        """
        Get all docs in a doc list
        """
        # User must be authenticated to get docs
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to get docs!"
            }, status=401)

        # Get all docs in a doc list
        if doc_list_id := kwargs.get('doc_list_id', None):
            if request.path == reverse('get_all_docs_from_doc_list', kwargs={'doc_list_id': doc_list_id}):
                doc_list = DocList.objects.get(id=doc_list_id)
                if doc_list.workspace.created_by != request.user and request.user not in doc_list.workspace.members.all():
                    return JsonResponse({
                        "message": "You do not have permission to access this list!"
                    }, status=403)

                docs = doc_list.doc_set.all()
                docs = [doc.to_dict() for doc in docs]

                # Return docs
                return JsonResponse({
                    "docs": docs
                }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)

    def post(self, request, **kwargs):
        """
        Create a new doc
        """
        # User must be authenticated to create a doc
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to create a doc!"
            }, status=401)

        # Create a new doc
        if request.path == reverse('create_docs'):
            data = json.loads(request.body)
            form = DocForm(data)
            if form.is_valid():
                # Owner of the doc list must be the same as the authenticated user
                doc_list = form.cleaned_data['doc_list']
                if doc_list.workspace.created_by != request.user and request.user not in doc_list.workspace.members.all():
                    return JsonResponse({
                        "message": "You do not have permission to create a doc in this list!"
                    }, status=403)

                doc = form.save()
                doc.created_by = request.user
                doc.contributors.add(request.user)
                doc.save()

                # Return success message
                return JsonResponse({
                    "message": f"Create doc {form.cleaned_data['name']} successfully!",
                    "doc": doc.to_dict()
                }, status=201)

        # Search for docs
        # User provide a search query in the request body
        if request.path == reverse('search_docs'):
            data = json.loads(request.body)
            query = data.get('query', '')

            docs = Doc.objects.filter(
                Q(name__icontains=query) |
                Q(content__icontains=query) |
                Q(tags__icontains=query))

            # Filter out docs that the user does not have access to
            user_workspaces = request.user.workspaces.all()
            docs = [
                doc.to_dict()
                for doc in docs
                if doc.doc_list.workspace in user_workspaces
                or doc.doc_list.workspace.created_by == request.user]

            # Return search results
            return JsonResponse({
                "docs": docs
            }, status=200)

        # Return error message if POST data is invalid
        return JsonResponse({
            "message": f"Failed to create doc!",
            'errors': form.errors
        }, status=400)
        
    def put(self, request, **kwargs):
        """
        Update a doc
        """
        # User must be authenticated to update a doc
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to update a doc!"
            }, status=401)

        # Must provide doc_id as a param in path
        if 'doc_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc id"
            }, status=400)

        doc_id = kwargs['doc_id']

        # Update a doc
        if request.path == reverse('update_docs', kwargs={'doc_id': doc_id}):
            data = json.loads(request.body)
            doc = Doc.objects.get(id=doc_id)
            if doc.doc_list.workspace.created_by != request.user and request.user not in doc.doc_list.workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to update this doc!"
                }, status=403)

            form = DocForm(data, instance=doc)
            if form.is_valid():
                doc = form.save(commit=False)
                doc.contributors.add(request.user)
                doc.save()

                # Return success message
                return JsonResponse({
                    "message": f"Update doc {form.cleaned_data['name']} successfully!",
                    "doc": doc.to_dict()
                }, status=200)

            # Return error message if PUT data is invalid
            return JsonResponse({
                "message": f"Failed to update doc!",
                'errors': form.errors
            }, status=400)

        # Invalid path
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)
        
    def delete(self, request, **kwargs):
        """
        Delete a doc
        """
        # User must be authenticated to delete a doc
        if request.user.is_anonymous:
            return JsonResponse({
                "message": "You must be authenticated to delete a doc!"
            }, status=401)

        # Must provide doc_id as a param in path
        if 'doc_id' not in kwargs:
            return JsonResponse({
                "message": "Missing doc id"
            }, status=400)

        doc_id = kwargs['doc_id']

        # Delete a doc
        if request.path == reverse('delete_docs', kwargs={'doc_id': doc_id}):
            doc = Doc.objects.get(id=doc_id)
            if doc.doc_list.workspace.created_by != request.user and request.user not in doc.doc_list.workspace.members.all():
                return JsonResponse({
                    "message": "You do not have permission to delete this doc!"
                }, status=403)

            doc.delete()

            # Return success message
            return JsonResponse({
                "message": "Delete doc successfully!"
            }, status=200)

        # Return error message if path is invalid
        return JsonResponse({
            "message": "Invalid path!"
        }, status=400)
