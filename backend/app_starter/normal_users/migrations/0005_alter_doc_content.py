# Generated by Django 5.0.3 on 2024-03-24 07:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('normal_users', '0004_doc'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doc',
            name='content',
            field=models.TextField(blank=True, null=True),
        ),
    ]
