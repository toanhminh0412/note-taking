# pylint:
from django.http import HttpResponseRedirect

# Custom mixin: Redirect unauthenticated users to the login page if they are not logged in
# (not currently used anywhere, provided as another option to the authenticate_middleware)
class LoginRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.session.get('user', None):
            return HttpResponseRedirect("/login")
        return super().dispatch(request, *args, **kwargs)

# Custom mixin: Redirect normal users to the homepage if they access an admin page
class AdminRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.session.get('is_admin', None):
            return HttpResponseRedirect("/")
        return super().dispatch(request, *args, **kwargs)
