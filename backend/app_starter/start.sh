#!/bin/sh
# Migrate schema to the database
python manage.py migrate

# Start Gunicorn server
python3 manage.py createusers
gunicorn -w 4 --chdir /usr/src/app app_starter.wsgi:application --bind 0.0.0.0:8000 --timeout 1000