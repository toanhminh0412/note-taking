# pylint:
from django.contrib.auth.models import User
from django.conf import settings
from django.http import JsonResponse
from django.views import View
from django.middleware.csrf import get_token
from django.contrib.auth import login, logout

from exceptions import BadConfig


def authenticate(data):
    """
    This function authenticates users by check the HTTP request header. The HTTP request header,
    namely 'X-Forwarded-User', contains username of the user. If the username is found in Django's user
    database, the user is authenticated. Else, the user is not. Users in user database is created by the superuser
    throught '/admin' page manually for TESTING PURPOSES.

    Args:
        data: HTTP request headers

    Returns:
        Tuple:
            - User Id: an int if user is authenticated, None if not authenticated
            - admin: True if user is authenticated and an admin, else False.
    """
    # For testing, can edit 'AUTHX_USER_OVERRIDE' to replace the HTTP request headers in settings.py
    if settings.DEBUG and settings.CONFIG.get('AUTHX_USER_OVERRIDE'):
        username = settings.CONFIG.get('AUTHX_USER_OVERRIDE')
        try:
            user = User.objects.get(username=username)
            admin = user.is_staff
            return (user.id, admin)
        except User.DoesNotExist:
            return (None, False)

    # header should be 'X-Forwarded-User'
    header = settings.CONFIG['AUTHX_HTTP_HEADER_USER']
    if header is None:
        raise BadConfig('Missing definition for HTTP request header container user ID')

    # read the username passed in the header
    username = data.get(header, None)

    if username is None:
        # raise BadCall('Missing HTTP request header for user authentication')
        return (-1, False)

    # Authenticate and check if user is an admin
    try:
        user = User.objects.get(username=username)
        admin = user.is_staff
        return (user.id, admin)
    except User.DoesNotExist:
        return (None, False)

# Login Page: Unauthenticated users and first-time visiters will have to
# get through this view
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class LoginView(View):
    def dispatch(self, request, *args, **kwargs):
        # Check if user is already logged in
        if request.user.is_authenticated:
            x_forwarded_user = request.headers.get('X-Forwarded-User')
            if x_forwarded_user and x_forwarded_user == request.user.username:
                return JsonResponse({
                    "message": "Logged in successfully. You're an user.",
                    "user_email": request.user.username,
                    "is_admin": request.user.is_staff,
                }, status=200)
            else:
                logout(request)

        # authenticate the user and check if the user is an admin
        (user_id, is_admin) = authenticate(request.headers)

        if user_id and user_id > 0:
            user = User.objects.get(id=user_id)
            login(request, user)
            print("Got here")
            print(request.user)
            
            # Generate a response
            response = {
                "user_email": user.username,
                "is_admin": is_admin,
            }
            if is_admin:
                request.session['is_admin'] = True
                response["message"] = "Logged in successfully. You're an admin."
                return JsonResponse(response, status=200)
            
            response["message"] = "Logged in successfully. You're an user."
            return JsonResponse(response, status=200)

        return JsonResponse({
            "message": "Failed to authenticate. Please contact us for support.",
        },
        status=401)

# Logging users out
# View class: https://docs.djangoproject.com/en/4.1/ref/class-based-views/base/
class LogoutView(View):
    # Disable arguments-differ as the missing argumentes are unused
    # pylint: disable=arguments-differ
    def dispatch(self, request):
        logout(request)

        # For TESTING ONLY
        if settings.DEBUG:
            settings.CONFIG['AUTHX_USER_OVERRIDE'] = ""

        return JsonResponse({"message": "Logged out successfully."}, status=200)
