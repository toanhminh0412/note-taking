// This function takes a list of classes and returns a string of classes separated by a space
export const classNames = (...classes) => {
    return classes.filter(Boolean).join(' ')
}