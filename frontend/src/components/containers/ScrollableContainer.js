import { useRef, useEffect } from 'react';

// A generic container that allows for horizontal scrolling by clicking and dragging.
export default function ScrollableContainer({ children, className }) {
    const containerRef = useRef(null);

    useEffect(() => {
        const container = containerRef.current;
        if (!container) return;

        const handleMouseDown = (e) => {
            if (!e.target.className || !e.target.getAttribute('class').includes('scrollable-container')) return; // Only drag when clicking on the container itself (not its children

            e.preventDefault(); // Prevent text selection.
            const startX = e.pageX;
            const startScrollLeft = container.scrollLeft;

            const handleMouseMove = (moveEvent) => {
                const dx = moveEvent.pageX - startX;
                container.scrollLeft = startScrollLeft - dx;
            };

            const handleMouseUp = () => {
                document.removeEventListener('mousemove', handleMouseMove);
                document.removeEventListener('mouseup', handleMouseUp);
            };

            document.addEventListener('mousemove', handleMouseMove);
            document.addEventListener('mouseup', handleMouseUp);
        };

        container.addEventListener('mousedown', handleMouseDown);

        // Cleanup function to remove event listeners
        return () => {
            container.removeEventListener('mousedown', handleMouseDown);
        };
    }, []);

    return (
        <div
        ref={containerRef}
        className={`scrollable-container ${className}`}
        >
        {children}
        </div>
    );
};
