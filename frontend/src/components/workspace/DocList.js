"use client";

import { Fragment, useState, useEffect, useContext, createContext, useRef, use } from "react";
import { Menu, Transition } from '@headlessui/react';
import { EllipsisHorizontalIcon } from '@heroicons/react/20/solid';
import { DragDropContext, Droppable, Draggable } from "@hello-pangea/dnd";
import CustomDialog from "../CustomDialog";
import useOutsideClick from "@/hooks/useOutsideClick";
import { Doc, CreateDocBtn } from "./Doc";
import { classNames } from "@/helper";

const DocListsContext = createContext();

export function DocListsDisplay({ workspace }) {
    const [docLists, setDocLists] = useState([]);

    useEffect(() => {
        if (workspace.id) {
            const fetchDocLists = async () => {
                const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/doc_lists/get_all_from_workspace/${workspace.id}`, {
                    method: 'GET',
                    credentials: 'include',
                });
                const data = await res.json();

                if (res.ok) {
                    console.log('DocLists fetched');
                    console.log(data);
                    setDocLists(data.doc_lists);
                } else {
                    console.error('Failed to fetch docLists');
                    console.log(data.errors);
                }
            }

            fetchDocLists();
        }
    }, [workspace]);

    const onDragDocEnd = async(result) => {
        const { destination, source, draggableId, type } = result;
        
        // Do nothing if the draggable item is dropped outside of a droppable
        if (!destination) return;

        // Do nothing if the draggable item is dropped in the same place
        if (destination.droppableId === source.droppableId && destination.index === source.index) return;

        // Handle doc lists reordering
        if (type === 'doclist') {
            // Do nothing if the doc list is dropped in the same place
            if (destination.index === source.index) return;

            // Keep a copy of the old doc lists to revert to if the update fails
            const oldDocLists = docLists;

            // Update the position of the dragged doc list on the frontend
            const newDocLists = Array.from(docLists);
            const docList = newDocLists.splice(source.index, 1);
            newDocLists.splice(destination.index, 0, docList[0]);
            newDocLists.forEach((docList, index) => docList.position = index + 1);
            setDocLists(newDocLists);

            // Update the position of the dragged doc list on the backend
            const docListIds = newDocLists.map(dl => dl.id);
            try {
                const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/workspaces/update_doc_lists_position/${workspace.id}`, {
                    method: 'PUT',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        docListIds: docListIds,
                    })
                });
                const data = await res.json();

                if (res.ok) {
                    console.log('DocList position updated');
                    console.log(data);
                } else {
                    console.error('Failed to update docList position');
                    console.log(data.errors);
                    // Revert to the old doc lists
                    setDocLists(oldDocLists);
                }
            } catch (error) {
                console.error('Failed to update docList position');
                console.log(error);
                // Revert to the old doc lists
                setDocLists(oldDocLists);
            }


            return;
        }

        // Handle reordering of docs in the same doc list
        const docId = parseInt(draggableId.split('-')[1]);
        if (destination.droppableId === source.droppableId) {
            const docListId = parseInt(source.droppableId.split('-')[1]);
            const docList = docLists.find(dl => dl.id === docListId);
            const newPosition = docList.docs[destination.index].position;
            
            // Keep a copy of the old doc list to revert to if the update fails
            const oldDocList = docList;

            // Update the position of the dragged doc on the frontend
            const sourceDoc = docList.docs.splice(source.index, 1);
            docList.docs.splice(destination.index, 0, sourceDoc[0]);
            setDocLists(docLists.map(dl => dl.id === docListId ? docList : dl));

            // Update the position of the dragged doc on the backend
            const docIds = docList.docs.map(doc => doc.id);
            try {
                const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/doc_lists/update_docs_position/${docListId}`, {
                    method: 'PUT',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        docIds: docIds,
                    })
                });
                const data = await res.json();

                if (res.ok) {
                    console.log('Doc position updated');
                    console.log(data);
                } else {
                    console.error('Failed to update doc position');
                    console.log(data.errors);
                    // Revert to the old doc list
                    setDocLists(docLists.map(dl => dl.id === docListId ? oldDocList : dl));
                }
            } catch (error) {
                console.error('Failed to update doc position');
                console.log(error);
                // Revert to the old doc list
                setDocLists(docLists.map(dl => dl.id === docListId ? oldDocList : dl));
            }
        }

        // Handle moving docs between doc lists
        if (destination.droppableId !== source.droppableId) {
            const sourceDocListId = parseInt(source.droppableId.split('-')[1]);
            const destinationDocListId = parseInt(destination.droppableId.split('-')[1]);
            const sourceDocList = docLists.find(dl => dl.id === sourceDocListId);
            const destinationDocList = docLists.find(dl => dl.id === destinationDocListId);

            // Keep a copy of the old doc lists to revert to if the update fails
            const oldSourceDocList = sourceDocList;
            const oldDestinationDocList = destinationDocList;

            // Update the position of the dragged doc on the frontend
            const sourceDoc = sourceDocList.docs.splice(source.index, 1);
            sourceDocList.docs.forEach((doc, index) => doc.position = index + 1);
            destinationDocList.docs.splice(destination.index, 0, sourceDoc[0]);
            destinationDocList.docs.forEach((doc, index) => doc.position = index + 1);
            setDocLists(docLists.map(dl => dl.id === sourceDocListId ? sourceDocList : dl));
            setDocLists(docLists.map(dl => dl.id === destinationDocListId ? destinationDocList : dl));

            // Update the position of the dragged doc on the backend
            const sourceDocIds = sourceDocList.docs.map(doc => doc.id);
            const destinationDocIds = destinationDocList.docs.map(doc => doc.id);
            try {
                const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/doc_lists/move_doc_to_list/${destinationDocListId}`, {
                    method: 'PUT',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        docId: docId,
                        sourceDocIds: sourceDocIds,
                        destinationDocIds: destinationDocIds,
                    })
                });
                const data = await res.json();
    
                if (res.ok) {
                    console.log('Doc position updated');
                    console.log(data);
                } else {
                    console.error('Failed to update doc position');
                    console.log(data.errors);
                    // Revert to the old doc lists
                    setDocLists(docLists.map(dl => dl.id === sourceDocListId ? oldSourceDocList : dl));
                    setDocLists(docLists.map(dl => dl.id === destinationDocListId ? oldDestinationDocList : dl));
                }
            } catch (error) {
                console.error('Failed to update doc position');
                console.log(error);
                // Revert to the old doc lists
                setDocLists(docLists.map(dl => dl.id === sourceDocListId ? oldSourceDocList : dl));
                setDocLists(docLists.map(dl => dl.id === destinationDocListId ? oldDestinationDocList : dl));
            }
        }
    }

    return (
        <DragDropContext onDragEnd={onDragDocEnd}>
            <DocListsContext.Provider value={{docLists, setDocLists}}>
                <Droppable droppableId={`workspace-${workspace.id}`} direction="horizontal" type="doclist">
                    {provided => (
                    <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        className="scrollable-container flex flex-row gap-4 items-start">
                        {docLists.map((docList, docListIndex) => (
                            <DocListCard key={docList.id} docList={docList} index={docListIndex}/>
                        ))}
                        <CreateListBtn workspace={workspace}/>
                        {provided.placeholder}
                    </div>)}
                </Droppable>
            </DocListsContext.Provider>
        </DragDropContext>
    )
}

export const DocsContext = createContext();

const DocListCard = ({ docList, index }) => {
    const { docLists, setDocLists } = useContext(DocListsContext);
    const docListNameRef = useRef(null);
    const [docs, setDocs] = useState(docList.docs);
    const [editName, setEditName] = useState(false);

    // Close the name edit field when clicking outside
    useOutsideClick(docListNameRef, () => setEditName(false));

    useEffect(() => {
        setDocs(docList.docs);
    }, [docList]);

    const updateDocList = async(e) => {
        e.preventDefault();
        const name = e.target.name.value;
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/doc_lists/update/${docList.id}`, {
            method: 'PUT',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                workspace: docList.workspace
            })
        });
        const data = await res.json();

        if (res.ok) {
            console.log('DocList updated');
            console.log(data);
            const newDocList = data.doc_list;

            // Close the name edit field
            setEditName(false);

            // Update the doc list's name
            setDocLists(docLists.map(dl => dl.id === newDocList.id ? newDocList : dl));
        } else {
            console.error('Failed to update docList');
            console.log(data.errors);
        }
    }

    return (
        <DocsContext.Provider value={{docs, setDocs}}>
            <Draggable draggableId={`doclist-${docList.id}`} index={index}>
                {provided => (
                <div 
                    {...provided.draggableProps}
                    ref={provided.innerRef}
                    className="bg-white rounded-md shadow-md border border-slate-100 p-2 w-60 h-fit">
                    <div 
                        {...provided.dragHandleProps}
                        className="flex flex-row justify-between gap-x-2">
                        <div className="grow">
                        {editName ? 
                            <form className="flex flex-row gap-2" onSubmit={updateDocList} ref={docListNameRef}>
                            <input 
                                type="text"
                                name="name"
                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2" 
                                placeholder="List name"
                                defaultValue={docList.name}
                                required />
                            </form>
                            :
                            <h5 className="text-sm font-medium text-slate-900 cursor-pointer"
                                onClick={() => setEditName(true)}>
                                    {docList.name}</h5>}
                        </div>
                        <DocListDropdown docList={docList} />
                    </div>
                    <Droppable droppableId={`doclist-${docList.id}`} type="doc">
                        {(provided) => 
                            <div 
                            className="flex flex-col gap-1 mt-2"
                            ref={provided.innerRef}
                            {...provided.droppableProps}>
                                {docs.map((doc, docIndex) => <Doc key={doc.id} doc={doc} index={docIndex}/>)}
                                {provided.placeholder}
                                <CreateDocBtn docList={docList}/>
                            </div>}
                    </Droppable>
                </div>
                )}
            </Draggable>
        </DocsContext.Provider>
    )
}

export function CreateListBtn({ workspace }) {
    const [state, setState] = useState('view');
    const {docLists, setDocLists} = useContext(DocListsContext);
    const createListFormRef = useRef(null);

    // Close the list creation form when clicking outside
    useOutsideClick(createListFormRef, () => setState('view'));

    const createDocList = async (e) => {
        e.preventDefault();
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/doc_lists/create`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: e.target.name.value,
                workspace: workspace.id,
            })
        });
        const data = await res.json();

        if (res.ok) {
            console.log('DocList created');
            console.log(data);
            // Render the new doc list
            setDocLists([...docLists, data.doc_list]);

            // Clear and close form
            e.target.reset();
            setState('view');
        } else {
            console.error('Failed to create docList');
            console.log(data.errors);
        }
    }

    if (state === "edit") {
        return (
            <div className="bg-white rounded-md shadow-md border border-slate-100 p-4 w-60 h-fit">
                <form onSubmit={createDocList} ref={createListFormRef}>
                    <div className="mb-3">
                        <input 
                            type="text"
                            name="name"
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2" 
                            placeholder="List name" 
                            required />
                    </div>
                    <button 
                        type="submit" 
                        className="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 font-medium rounded-lg text-xs px-3 py-2 text-center me-2">
                        <i className="fa-solid fa-plus me-2"></i>
                        Create list
                    </button>
                    <button 
                        type="button" 
                        className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-xs px-3 py-2 text-center me-2"
                        onClick={() => setState("view")}>
                        Cancel
                    </button>
                </form>
            </div>
        )
    }

    return (
        <button 
            type="button" 
            className="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 font-medium rounded-md text-sm p-2 text-center w-60"
            onClick={() => setState("edit")}>
            <i className="fa-solid fa-plus me-2"></i>
            Create another list
        </button>
    )
}

export function DocListDropdown({ docList }) {
    const docListNameLowercase = docList.name.toLowerCase().replace(/\s+/g, '');

    const {docLists, setDocLists} = useContext(DocListsContext);
    const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
    const [deleteInput, setDeleteInput] = useState('');
    const [isDeleting, setIsDeleting] = useState(false);

    const deleteDocList = async (e) => {
        e.preventDefault();

        // Do nothing in the confirm input is incorrect
        if (deleteInput !== docListNameLowercase) return;

        setIsDeleting(true);

        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/doc_lists/delete/${docList.id}`, {
            method: 'DELETE',
            credentials: 'include',
        });
        const data = await res.json();

        if (res.ok) {
            console.log('DocList deleted');
            console.log(data);

            // Remove the doc list displayed in the workspace
            setDocLists(docLists.filter(dl => dl.id !== docList.id));

            // Close the dialog
            setIsDeleteDialogOpen(false);
        } else {
            console.error('Failed to delete docList');
            console.log(data.errors);

        }

        // Disable the loading state
        setIsDeleting(false);
    }

    return (
        <>
            <Menu as="span" className="relative">
                <Menu.Button>
                    <EllipsisHorizontalIcon className="menu-icon block w-5 h-5 ml-auto my-auto rounded-md duration-75 hover:text-black hover:bg-gray-200"/>
                </Menu.Button>
                <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                    <Menu.Items className="absolute left-[100%] top-0 w-60 z-10 mt-2.5 py-2 origin-top-right rounded-sm bg-white shadow-lg ring-1 ring-gray-900/5 focus:outline-none">
                        <Menu.Item>
                            <span className='block px-3 py-1 text-sm text-center font-semibold leading-6 text-gray-600 mb-3'>{docList.name}</span>
                        </Menu.Item>
                        <Menu.Item onClick={() => setIsDeleteDialogOpen(true)}>
                        {({ active }) => (
                            <span
                                className={classNames(
                                    active ? 'bg-gray-100' : '',
                                    'block px-3 py-1 text-sm leading-6 text-gray-900 font-medium cursor-pointer'
                                )}
                                >
                                Delete list
                            </span>
                        )}
                        </Menu.Item>
                    </Menu.Items>
                </Transition>
            </Menu>

            {/* Dialog for deleting a workspace */}
            <CustomDialog isOpen={isDeleteDialogOpen} setIsOpen={setIsDeleteDialogOpen} title="Delete doc list" size="lg">
                <div className="block py-2 text-sm">Deleting this doc list will delete all docs and tags associated with it. To confirm deleting this doc list, type <strong>{docListNameLowercase}</strong> below:</div>
                <form onSubmit={deleteDocList}>
                    <input 
                        type="text" 
                        className="block w-full border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5" 
                        placeholder={`Type "${docListNameLowercase}"`}
                        value={deleteInput}
                        onChange={(e) => setDeleteInput(e.target.value)}
                        required
                    ></input>
                    <button 
                        type="submit" 
                        className={`text-xs text-white w-full ${deleteInput !== docListNameLowercase ? "bg-red-200" : "bg-red-500 hover:bg-red-700"} rounded-md py-2 px-4 mt-4`}
                        disabled={deleteInput !== docListNameLowercase}>
                        {isDeleting ? 'Deleting...' : 'Delete'}
                    </button>
                </form>
            </CustomDialog>
        </>
    )
}