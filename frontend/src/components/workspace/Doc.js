"use client";

import Markdown from "react-markdown";
import remarkGfm from "remark-gfm";
import CustomDialog from "../CustomDialog";

import { Menu, Transition } from '@headlessui/react';
import { EllipsisHorizontalIcon } from '@heroicons/react/20/solid';
import { classNames } from "@/helper";

import { Fragment, useState, useContext, useEffect, useRef } from "react";
import { useRouter, useSearchParams, usePathname } from "next/navigation";
import { Draggable } from "@hello-pangea/dnd"

import useOutsideClick from "@/hooks/useOutsideClick";
import { WorkspaceContext, SelectedTagsContext } from "@/app/workspace/[workspaceId]/page";
import { DocsContext } from "./DocList";

export const Doc = ({ doc, index }) => {
    const {workspace, setWorkspace} = useContext(WorkspaceContext);
    const {selectedTags, setSelectedTags} = useContext(SelectedTagsContext);
    const {docs, setDocs} = useContext(DocsContext);

    const router = useRouter();
    const pathname = usePathname();

    // Get search params
    const searchParams = useSearchParams();
    const searchedDoc = parseInt(searchParams.get('doc'));

    // State to keep track of doc state in form
    const [currentDoc, setCurrentDoc] = useState(doc);

    // Edit or preview Markdown content
    const [contentState, setContentState] = useState(doc.content ? 'preview' : 'edit');

    // Are we waiting for the doc to update
    const [isUpdating, setIsUpdating] = useState(false);

    // Is the doc modal open
    const [isOpen, setIsOpen] = useState(searchedDoc === doc.id);

    useEffect(() => {
        setIsOpen(searchedDoc === doc.id);
    }, [searchedDoc]);

    // Modify doc name on type
    const updateDocName = (e) => {
        setCurrentDoc({
            ...currentDoc,
            name: e.target.value
        });
    }

    // Modify doc content on type
    const updateDocContent = (e) => {
        setCurrentDoc({
            ...currentDoc,
            content: e.target.value
        });
    }

    // Add tag
    const addTag = (e) => {
        e.preventDefault();
        const tag = e.target.tags.value;
        setCurrentDoc({
            ...currentDoc,
            tags: [...currentDoc.tags, tag]
        });
        e.target.reset();
    }

    // Delete tag
    const deleteTag = (tag) => {
        setCurrentDoc({
            ...currentDoc,
            tags: currentDoc.tags.filter(t => t !== tag)
        });
    }

    // Update doc
    const updateDoc = async (e) => {
        e.preventDefault();
        setIsUpdating(true);
        
        try {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/docs/update/${doc.id}`, {
                method: 'PUT',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: currentDoc.name,
                    content: currentDoc.content,
                    tags: currentDoc.tags.join(','),
                    doc_list: currentDoc.doc_list
                })
            });

            const data = await res.json();
            setIsUpdating(false);

            if (res.ok) {
                console.log('Doc updated');
                console.log(data);
                // Close modal
                setIsOpen(false);

                // Update doc in the list
                // Add new tags into workspace tags
                let workspaceTags = workspace.tags;
                data.doc.tags.forEach(tag => {
                    if (!workspaceTags.includes(tag)) {
                        workspaceTags.push(tag);
                    }
                });
                console.log(workspaceTags);
                setWorkspace({...workspace, tags: workspaceTags});
                setDocs(docs.map(d => d.id === doc.id ? data.doc : d));
            } else {
                console.error('Failed to update doc');
                console.log(data.errors);
            }
        } catch (error) {
            setIsUpdating(false);
            console.error('Failed to update doc');
            console.log(error);
        }
    }

    return (
        <>
            <Draggable draggableId={`doc-${doc.id}`} index={index}>
                {/* Doc display in a list */}
                {(provided) => (
                    <div 
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    ref={provided.innerRef}
                    className="relative group"
                    >
                        <div 
                            className={`${selectedTags.length === 0 || doc.tags.some(tag => selectedTags.includes(tag)) ? "" : "hidden"} w-full p-2 rounded-md text-sm cursor-pointer bg-slate-200 border-2 border-slate-200 hover:border-blue-500`}
                            onClick={() => setIsOpen(true)}
                            type="button">
                            {doc.name}
                        </div>
                        <DocDropdown doc={doc}/>
                    </div>
                )}
            </Draggable>

            {/* Modal to update/view a doc */}
            <CustomDialog isOpen={isOpen} setIsOpen={setIsOpen} title={currentDoc.name} size="7xl" onClose={() => router.push(`/workspace/${workspace.id}`)}>
                <div className="grid gap-4 mb-4 grid-cols-2">
                    
                    {doc.created_by || doc.contributors.length > 0 ?
                    <div className="col-span-2">
                        {doc.created_by ? 
                        <div className="text-xs text-gray-500">
                            Created by {doc.created_by} on {new Date(doc.created_at).toLocaleString()}.
                        </div>: null}
                        {doc.contributors.length > 0 ?
                        <div className="text-xs text-gray-500">
                            Contributors: {doc.contributors.join(', ')}
                        </div> : null
                        }
                    </div> : null}
                    <div className="col-span-2">
                        <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900">Name</label>
                        <input 
                            type="text" 
                            name="name" 
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5" 
                            placeholder="Document name" 
                            value={currentDoc.name}
                            onChange={updateDocName}
                            required/>
                    </div>
                    <div className="col-span-2">
                        <label htmlFor="description" className="block mb-2 text-sm font-medium text-gray-900">Documentation content (Markdown supported)</label>
                        <div className="inline-flex rounded-md shadow-sm mb-2" role="group">
                            <button 
                                type="button" 
                                className={`inline-flex items-center px-4 py-2 text-sm font-medium ${contentState === 'edit' ? "text-blue-700 bg-gray-100" : "text-gray-900 bg-white"} border border-gray-200 rounded-s-lg hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700`}
                                onClick={() => setContentState("edit")}>
                                <i className="fa-solid fa-pen me-2"></i>
                                Edit
                            </button>
                            <button 
                                type="button" 
                                className={`inline-flex items-center px-4 py-2 text-sm font-medium  ${contentState === 'preview' ? "text-blue-700 bg-gray-100" : "text-gray-900 bg-white"} border border-gray-200 rounded-e-lg hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700`}
                                onClick={() => setContentState("preview")}>
                                <i className="fa-solid fa-eye me-2"></i>
                                Preview
                            </button>
                        </div>
                        {contentState === "edit" ? 
                        <textarea 
                            rows="20" 
                            className="block p-4 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500" 
                            placeholder="Documentation content. Markdown supported"
                            onChange={updateDocContent}
                            value={currentDoc.content}></textarea> : 
                        <div className="p-4 bg-gray-50 rounded-lg border border-gray-300 prose max-w-none">
                            <Markdown remarkPlugins={[remarkGfm]}>{currentDoc.content}</Markdown>
                        </div>}
                        {/* Tags */}
                        <div className="mt-4">
                            <label htmlFor="tags" className="block mb-2 text-sm font-medium text-gray-900">Tags</label>
                            <form onSubmit={addTag}>
                                <input 
                                    type="text" 
                                    name="tags" 
                                    className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5" 
                                    placeholder="Tags" />
                            </form>
                            {/* Render existing tags */}
                            <div className="mt-2">
                                {currentDoc.tags.map(tag => (
                                <span key={tag} className="inline-flex items-center px-2.5 py-1.5 text-xs font-medium bg-gray-100 text-gray-900 rounded-sm me-2">
                                    {tag}
                                    <button 
                                        type="button" 
                                        className="text-gray-400 hover:text-gray-600"
                                        onClick={() => deleteTag(tag)}>
                                        <i className="fa-solid fa-xmark ms-2"></i>
                                    </button>
                                </span>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
                {!isUpdating ? <button 
                    type="submit" 
                    className="text-white inline-flex items-center bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 text-center"
                    onClick={updateDoc}>
                    <i className="fa-solid fa-floppy-disk me-2"></i>
                    Save doc
                </button> : 
                <button 
                    type="button" 
                    className="text-white bg-gray-400 cursor-not-allowed font-medium rounded-lg text-sm px-5 py-2.5 me-2 text-center">
                    <i className="fa-solid fa-spinner me-2"></i>
                    Saving...
                </button>}
                <button 
                    type="button" 
                    className="text-white bg-red-600 hover:bg-red-700 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                    onClick={() => {setIsOpen(false); router.push(pathname);}}>
                    <i className="fa-solid fa-xmark me-2"></i>
                    Cancel
                </button>
            </CustomDialog>
        </>
    )
}

const DocDropdown = ({ doc }) => {
    const {docs, setDocs} = useContext(DocsContext);
    const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
    const [isDeleting, setIsDeleting] = useState(false);

    // Delete doc
    const deleteDoc = async (e) => {
        e.preventDefault();
        setIsDeleting(true);

        try {
            const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/docs/delete/${doc.id}`, {
                method: 'DELETE',
                credentials: 'include'
            });
            const data = await res.json();
            setIsDeleting(false);

            if (res.ok) {
                console.log('Doc deleted');
                console.log(data);

                // Close dialog
                setIsDeleteDialogOpen(false);

                // Remove doc from the list
                setDocs(docs.filter(d => d.id !== doc.id));
            } else {
                console.error('Failed to delete doc');
                console.log(data.errors);
            }
        } catch (error) {
            setIsDeleting(false);
            console.error('Failed to delete doc');
            console.log(error);
        }
    }

    return (
        <div className="absolute top-2 right-2">
            <Menu as="span" className="relative">
                <Menu.Button>
                    <EllipsisHorizontalIcon className="hidden group-hover:block menu-icon w-5 h-5 ml-auto my-auto rounded-md duration-75 bg-white hover:text-black hover:bg-gray-200"/>
                </Menu.Button>
                <Transition
                    as={Fragment}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                    <Menu.Items className="absolute left-[100%] top-0 w-60 z-10 mt-2.5 py-2 origin-top-right rounded-sm bg-white shadow-lg ring-1 ring-gray-900/5 focus:outline-none">
                        <Menu.Item>
                            <span className='block px-3 py-1 text-sm text-center font-semibold leading-6 text-gray-600 mb-3'>{doc.name}</span>
                        </Menu.Item>
                        <Menu.Item onClick={() => setIsDeleteDialogOpen(true)}>
                        {({ active }) => (
                            <span
                                className={classNames(
                                    active ? 'bg-gray-100' : '',
                                    'block px-3 py-1 text-sm leading-6 text-gray-900 font-medium cursor-pointer'
                                )}
                                >
                                Delete doc
                            </span>
                        )}
                        </Menu.Item>
                    </Menu.Items>
                </Transition>
            </Menu>

            {/* Dialog for deleting a workspace */}
            <CustomDialog isOpen={isDeleteDialogOpen} setIsOpen={setIsDeleteDialogOpen} title="Delete doc list" size="lg">
                <div className="block py-2 text-sm">Confirm deleting doc <strong>{doc.name}</strong></div>
                <form onSubmit={deleteDoc}>
                    <button 
                        type="submit" 
                        className={`text-xs text-white w-full bg-red-500 hover:bg-red-700 rounded-md py-2 px-4 mt-4`}>
                        {isDeleting ? 'Deleting...' : 'Delete'}
                    </button>
                </form>
            </CustomDialog>
        </div>
    )
}

export const CreateDocBtn = ({ docList }) => {
    const [state, setState] = useState('view');
    const {docs, setDocs} = useContext(DocsContext);
    const createDocFormRef = useRef(null);

    // Close doc creation form when clicked outside
    useOutsideClick(createDocFormRef, () => setState('view'));

    const createDoc = async (e) => {
        e.preventDefault();
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/docs/create`, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: e.target.name.value,
                doc_list: docList.id
            })
        });
        const data = await res.json();

        if (res.ok) {
            console.log('Doc created');
            console.log(data);

            // Render the new doc
            setDocs([...docs, data.doc]);

            // Clear and close form
            e.target.reset();
            setState('view');
        } else {
            console.error('Failed to create doc');
            console.log(data.errors);
        }
    }

    if (state === "edit") {
        return (
            <div className="w-full p-1 rounded-md">
                <form onSubmit={createDoc} ref={createDocFormRef}>
                    <div className="mb-3">
                        <textarea 
                            type="text"
                            name="name"
                            rows={2}
                            className="bg-gray-50 border border-gray-300 text-gray-900 text-xs rounded-md focus:ring-blue-500 focus:border-blue-500 block w-full p-2" 
                            placeholder="Enter a name for this documentation..." 
                            required />
                    </div>
                    <button 
                        type="submit" 
                        className="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 font-medium rounded-lg text-xs px-3 py-2 text-center me-2">
                        <i className="fa-solid fa-plus me-2"></i>
                        Create doc
                    </button>
                    <button 
                        type="button" 
                        className="text-white bg-gradient-to-r from-red-400 via-red-500 to-red-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-xs px-3 py-2 text-center me-2"
                        onClick={() => setState("view")}>
                        Cancel
                    </button>
                </form>
            </div>
        )
    }

    return (
        <div 
            className="w-full p-2 rounded-md hover:bg-slate-200 text-sm cursor-pointer"
            onClick={() => setState('edit')}>
            <i className="fa-solid fa-plus me-2"></i>
            Add a documetation
        </div>
    )
}