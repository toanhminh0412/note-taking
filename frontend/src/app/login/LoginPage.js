"use client";

import { useEffect } from "react";

export default function LoginPage({ user }) {
    const login = async () => {
        const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
            headers: {
                "X-Forwarded-User": user,
            },
            credentials: "include"
        });
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            localStorage.setItem("email", data.user_email);
            localStorage.setItem("isAdmin", data.is_admin);
            window.location.href = "/";
        } else {
            console.error("Failed to authenticate");
        }
    }

    useEffect(() => {
        login();
    })

    return (
        <main className="mt-20 prose p-4">
            <h1>Logging in...</h1>
        </main>
    )
}