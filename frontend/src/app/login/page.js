import { headers } from 'next/headers'
import LoginPage from "./LoginPage";

export default function Page() {
    const headersList = headers();
    let user = headersList.get("X-Forwarded-User");

    if (!user && process.env.NEXT_PUBLIC_USER_OVERRIDE) {
        user = process.env.NEXT_PUBLIC_USER_OVERRIDE;
    }

    return (
        <LoginPage user={user}/>
    );
}