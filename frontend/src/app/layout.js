import { Inter } from "next/font/google";
import "./globals.css";

import AppRootLayout from "@/components/layouts/AppRootLayout";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
    title: "ARC Notes",
    description: "A simple notes app for ARC software",
};

export default function RootLayout({ children }) {
    return (
        <html lang="en" className="h-full bg-white">
            <head>
                {/* Font Awesome CSS */}
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossOrigin="anonymous" referrerPolicy="no-referrer" />
            </head>
            <body className={`${inter.className} h-full`}>
                <AppRootLayout>
                    {children}
                </AppRootLayout>
            </body>
        </html>
    );
}
