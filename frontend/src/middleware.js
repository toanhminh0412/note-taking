import { NextResponse } from 'next/server'
import { headers, cookies } from 'next/headers'
 
// This function can be marked `async` if using `await` inside
export async function middleware(request) {
    // Header X-Forwared-User is passed from Keycloak
    // We use this header to authenticate with the Django backend
    const cookieStore = cookies();

    // Authenticate user with Django backend if not already authenticated
    if (!cookieStore.get('sessionid')) {
        return NextResponse.redirect(new URL('/login', request.url));
    }

    return NextResponse.next();
}
 
// See "Matching Paths" below to learn more
export const config = {
    matcher: '/',
}